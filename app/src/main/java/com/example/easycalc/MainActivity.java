package com.example.easycalc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Crear los objetos que se "enlazarán" a los controles
    TextView resultado;
    EditText numero1;
    EditText numero2;
    Button bSumar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultado=findViewById(R.id.resultado);
        numero1=findViewById(R.id.numero1);
        numero2=findViewById(R.id.numero2);
        bSumar=findViewById(R.id.bSumar);
    }

    public void sumar(View v){

        //Guardar en la variable x el contenido del texto en numero1
        String valor= this.numero1.getText().toString();
        int x= Integer.parseInt(valor);

        valor= this.numero2.getText().toString();
        int y= Integer.parseInt(valor);

        int suma=x+y;

        this.resultado.setText(String.valueOf(suma));


    }
}